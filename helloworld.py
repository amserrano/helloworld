from flask import Flask, render_template
 
app = Flask(__name__)
 
@app.route("/")
def hello():
    capitals = {
        'Slovenia' : 'Ljubljana',
        'Serbia' : 'Belgrade',
        'Croatia' : 'Zagreb',
        'Albania' : 'Tirana',
        'Republic of Macedonia': 'Skopje'
    }
    return render_template('capitals.html', data=capitals)
 
def bootapp():
    app.run(port=8080, threaded=True, host=('0.0.0.0'))

if __name__ == '__main__':
     bootapp()
